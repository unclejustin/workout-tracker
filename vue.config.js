const path = require("path");

module.exports = {
  chainWebpack: config => {
    config.resolve.alias.set(
      "api",
      path.join(__dirname, "src", "api/json-server.js")
    );
  }
};
