# Workout Tracker

## Contributing

See [Contributing](CONTRIBUTING.md) doc for information on developing locally and contributing.

### Overview

Workout Tracker is a simple way to log workouts built using Vue CLI.
