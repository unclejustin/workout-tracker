import { shallowMount } from "@vue/test-utils";
import WorkoutEntryForm from "@/components/WorkoutEntryForm.vue";

describe("HelloWorld.vue", () => {
  it("renders props.msg when passed", () => {
    const workout = {};
    const wrapper = shallowMount(WorkoutEntryForm, {
      propsData: { workout }
    });
    expect(wrapper).toBeDefined();
  });
});
