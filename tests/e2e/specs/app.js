// https://docs.cypress.io/api/introduction/api.html
/// <reference types="Cypress" />
/// <reference types="@testing-library/cypress/typings" />

import dayjs from "dayjs";

describe("Workout Tracker", async () => {
  const exerciseSelector = /^exercise$/i;

  it("Visits the app root url", () => {
    cy.exec("npm run db:test:reset");

    cy.visit("/");
    cy.contains("a", /Workout Tracker/i);
  });

  it("Adds a new workout and navigates there", () => {
    cy.get("button")
      .contains(/workout/i)
      .click();

    cy.getByLabelText(/date/i).should(
      "have.value",
      dayjs().format("YYYY-MM-DD")
    );
  });

  describe("Workout entry form", async () => {
    const WORKOUT_A = "Workout A";
    const populateDate = date =>
      cy
        .getByLabelText(/date/i)
        .clear()
        .type(date);
    const populateTitle = title =>
      cy
        .getByLabelText(/title/i)
        .clear()
        .type(title);
    const populateExerciseInfo = () => {
      cy.getByLabelText(exerciseSelector).select("1");
      cy.getByLabelText(/period/i).select("1");
      cy.getByLabelText(/weight/i)
        .clear()
        .type("100");
      cy.getByLabelText(/reps/i)
        .clear()
        .type("15");
      cy.getByLabelText(/notes/i)
        .clear()
        .type("Test notes");
    };
    const populateExerciseForm = (date, title) => {
      if (date) populateDate(date);
      if (title) populateTitle(title);
      populateExerciseInfo();
    };

    const addExercise = (date, title) => {
      populateExerciseForm(date, title);
      cy.get("button")
        .contains(/add/i)
        .click();
    };

    it("Alerts and prevents submitting the form without all required fields", () => {
      const submitAndCheckMessage = () => {
        cy.get("button")
          .contains(/add/i)
          .click();

        cy.getAllByText(/you need to/i);
        cy.getByLabelText(/close/i).click();
      };

      submitAndCheckMessage();

      cy.getByLabelText(exerciseSelector).select("1");
      submitAndCheckMessage();
      cy.getByLabelText(exerciseSelector).select("Select an exercise");

      cy.getByLabelText(/period/i).select("1");
      submitAndCheckMessage();
      cy.getByLabelText(/period/i).select("Select a period");

      cy.getByLabelText(/weight/i).type("100");
      submitAndCheckMessage();
      cy.getByLabelText(/weight/i).clear();
    });

    it("Adds exercises", () => {
      cy.server();
      cy.route("GET", "http://localhost:3000/exercises**").as("loadExercises");

      addExercise("2019-01-01");
      addExercise(undefined, WORKOUT_A);

      cy.wait(["@loadExercises", "@loadExercises", "@loadExercises"]);
      cy.wait(100);

      cy.getAllByTestId("exercises").within(() => {
        cy.queryAllByText(/bench/i).should("have.length", 2);
      });

      cy.getAllByText(/100 lbs x 15/i);
    });

    it("Deletes an exercise", () => {
      cy.server();
      cy.route("GET", "http://localhost:3000/exercises**").as("loadExercises");

      cy.findAllByLabelText(/delete exercise/i)
        .first()
        .click();

      cy.wait("@loadExercises");
      cy.wait(1);

      cy.getAllByTestId("exercises").within(() => {
        cy.queryAllByText(/bench/i).should("have.length", 1);
      });
    });

    it("Undoes deleting an exercise", () => {
      cy.server();
      cy.route("GET", "http://localhost:3000/exercises**").as("loadExercises");

      cy.getByText(/undo last delete/i).click();

      cy.wait("@loadExercises");
      cy.wait(100);

      cy.getAllByTestId("exercises").within(() => {
        cy.queryAllByText(/bench/i).should("have.length", 2);
      });
    });

    it("Starts a brand new workout", () => {
      cy.get("button")
        .contains(/workout/i)
        .click();

      cy.contains(/bench/i).should("not.exist");

      cy.contains(/100 lbs x 15/i).should("not.exist");
    });

    it("Shows previous weight, reps, and notes", () => {
      cy.server();
      cy.route("http://localhost:3000/exercises**").as("prevExercise");
      cy.getByLabelText(exerciseSelector).select("1");
      cy.getByLabelText(/period/i).select("1");
      cy.wait("@prevExercise");
      cy.wait(1);

      cy.getByLabelText(/weight/i).should("have.attr", "placeholder", "100");
      cy.getByLabelText(/reps/i).should("have.attr", "placeholder", "15");
      cy.getByLabelText(/notes/i).should(
        "have.attr",
        "placeholder",
        "Test notes"
      );
    });

    it("Clears the form", () => {
      populateExerciseForm();
      cy.wait(1);

      cy.getByText(/clear/i).click();

      cy.getByLabelText(/weight/i).should("not.have.value");
      cy.getByLabelText(/reps/i).should("not.have.value");
      cy.getByLabelText(/notes/i).should("not.have.value");
    });
  });

  describe("Workout list", () => {
    const date1 = dayjs().format("dddd, MMM D, YYYY");
    const date2 = dayjs("2019-01-01").format("dddd, MMM D, YYYY");

    it("Navigate to the home page and list workouts", () => {
      cy.visit("/");
      cy.getByText(/past workouts/i);
      cy.getAllByTestId("workout-list-item").then(workoutItems => {
        cy.get(workoutItems[0]).getByText(date1, { exact: false });
        cy.get(workoutItems[1]).getByText(date2, { exact: false });
      });
    });

    it("Delete a workout", () => {
      cy.getAllByTitle(/delete workout/i)
        .first()
        .click();
      cy.getByText(/undo last delete/i);
      cy.queryByTitle(/delete workout/i).should("have.length", 1);
    });

    it("Undo deleting a workout and navigate there", () => {
      cy.getByText(/undo last delete/i).click();
      cy.queryByText(date2, { exact: false }).click();
    });
  });
});
