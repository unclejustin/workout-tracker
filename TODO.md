# TODO

- [x]Add placeholder component for main workout entry
- [x]Remove existing styling
- [x]Remove HelloWorld and assets
- [x]Use workout entry form as main component in default route
- [x]Add bootstrap-vue
- [x]Build static form
  - [x]Date selector: default to today
  - [X]Add exercise button
  - [x]Exercises
    - [x]Period selector
    - [x]Exercise selector
    - [x]Weight input
      - [x]If we have previous data for this period/exercise combo, show most recent weight and reps
        - [x]ex: 105 x 12, 10
        - [x]show previous notes also if available
- [x]Pull dropdowns from data
- [x]Add exercise
- [X]Update exercise list
- [x]Delete exercise button
- [x]Delete workout button
- [x]Save workout and reset for next one
- [x]Order workouts newest -> oldest
- [x]Order exercises newest -> oldest
- [x]Notes on exercises

## Cypress notes

- Fetch does not work
- Need to wait after wait for request for some reason
