import { fetch as fetchPolyfill } from "whatwg-fetch";

if (window.Cypress) {
  window.fetch = fetchPolyfill;
}

const apiUrl = "http://localhost:3000";

const fetchWrapper = (url, params) =>
  fetch(url, params).then(response => response.json());

const _get = url => fetchWrapper(url);

const _post = (url, data) => {
  const params = {
    body: JSON.stringify(data),
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    }
  };

  return fetchWrapper(url, params);
};

const _patch = (url, data) => {
  const params = {
    body: JSON.stringify(data),
    method: "PATCH",
    headers: {
      "Content-Type": "application/json"
    }
  };

  return fetchWrapper(url, params);
};

const _delete = url => {
  const params = {
    method: "DELETE",
    headers: {
      "Content-Type": "application/json"
    }
  };

  return fetchWrapper(url, params);
};

const newItem = item => ({
  ...item,
  createdOn: Date.now(),
  updatedOn: Date.now(),
  deletedOn: 0
});

const getExerciseList = () => _get(`${apiUrl}/exerciseList`);

const getPeriodList = () => _get(`${apiUrl}/periodList`);

const getExercisesByWorkout = id =>
  _get(
    `${apiUrl}/exercises?workoutId=${id}&deletedOn=0&_sort=createdOn&_order=desc`
  );

const addExercise = exercise => _post(`${apiUrl}/exercises`, newItem(exercise));

const addWorkout = workout => _post(`${apiUrl}/workouts`, newItem(workout));

const delExercise = id =>
  _patch(`${apiUrl}/exercises/${id}`, { deletedOn: Date.now() });

const updateExercise = exercise =>
  _patch(`${apiUrl}/exercises/${exercise.id}`, exercise);

const getWorkouts = () => {
  return _get(
    `${apiUrl}/workouts?_limit=25&_sort=date&_order=desc&deletedOn=0`
  );
};

const _getWorkout = id => _get(`${apiUrl}/workouts/${id}`);

const getWorkout = async id => {
  const [workout, exercises, exerciseList, periodList] = await Promise.all([
    _getWorkout(id),
    getExercisesByWorkout(id),
    getExerciseList(),
    getPeriodList()
  ]);

  return await {
    ...workout,
    exercises: exercises.map(exercise => {
      const matchedExercise = exerciseList.find(
        e => e.id === exercise.exerciseListId
      );
      const matchedPeriod = periodList.find(
        p => p.id === exercise.periodListId
      );

      if (!matchedExercise)
        throw new Error(
          `Couldn't find exercise on exercise(${exercise.id}) with id 
          ${exercise.exerciseListId}`
        );
      if (!matchedPeriod)
        throw new Error(
          `Couldn't find period on exercise (${exercise.id}) with id 
          ${exercise.periodListId}`
        );

      return {
        ...exercise,
        exerciseText: matchedExercise.text,
        periodText: matchedPeriod.text
      };
    })
  };
};

const getPreviousExercise = async (exerciseListId, periodListId) => {
  const exercises = await _get(
    `${apiUrl}/exercises?exerciseListId=${exerciseListId}&periodListId=${periodListId}&_sort=updatedOn&_order=desc`
  );

  return exercises[0];
};

const updateWorkout = async workout =>
  _patch(`${apiUrl}/workouts/${workout.id}`, workout);

const undoDelExercise = id =>
  _patch(`${apiUrl}/exercises/${id}`, { deletedOn: 0 });

const delExercisesByWorkout = async id => {
  const exercises = await getExercisesByWorkout(id);

  return Promise.all(exercises.map(e => delExercise(e.id)));
};

const undoDelExercisesByWorkout = async id => {
  const exercises = await getExercisesByWorkout(id);

  return Promise.all(exercises.map(e => undoDelExercise(e.id)));
};

const delWorkout = async id => {
  await Promise.all([
    _patch(`${apiUrl}/workouts/${id}`, { deletedOn: Date.now() }),
    delExercisesByWorkout(id)
  ]);
};

const undoDelWorkout = id => {
  return Promise.all([
    _patch(`${apiUrl}/workouts/${id}`, { deletedOn: 0 }),
    undoDelExercisesByWorkout(id)
  ]);
};

const purge = async table => {
  const url = `${apiUrl}/${table}`;
  const records = await _get(`${url}?deletedOn_gte=1`);
  return Promise.all(records.map(e => _delete(`${url}/${e.id}`)));
};

export default {
  addExercise,
  addWorkout,
  delExercise,
  delExercisesByWorkout,
  delWorkout,
  getExerciseList,
  getExercisesByWorkout,
  getPeriodList,
  getPreviousExercise,
  getWorkout,
  getWorkouts,
  purge,
  undoDelExercise,
  undoDelWorkout,
  updateExercise,
  updateWorkout
};
