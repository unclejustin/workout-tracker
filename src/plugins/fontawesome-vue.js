import Vue from "vue";
import {
  faPlusCircle,
  faTimesCircle,
  faBan,
  faUndo
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";

library.add([faPlusCircle, faTimesCircle, faBan, faUndo]);
Vue.component("font-awesome-icon", FontAwesomeIcon);
