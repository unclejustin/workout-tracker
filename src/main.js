import "@babel/polyfill";
import "mutationobserver-shim";
import Vue from "vue";
import "./plugins/bootstrap-vue";
import "./plugins/fontawesome-vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import { Promised } from "vue-promised";
import PromisedWrapper from "@/components/PromisedWrapper";
import { ToastPlugin } from "bootstrap-vue";

import "@/assets/custom.scss";

Vue.component("PromisedWrapper", PromisedWrapper);

Vue.config.productionTip = false;
Vue.component("Promised", Promised);
Vue.use(ToastPlugin);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
