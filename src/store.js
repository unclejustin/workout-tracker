import Vue from "vue";
import Vuex from "vuex";
import api from "api";

Vue.use(Vuex);

export const mutations = {
  ADD_EXERCISE: "ADD_EXERCISE",
  REMOVE_EXERCISE: "REMOVE_EXERCISE",
  LOAD_WORKOUT: "LOAD_WORKOUT"
};

export default new Vuex.Store({
  state: {
    workout: {
      exercises: []
    }
  },
  mutations: {
    [mutations.LOAD_WORKOUT](state, payload) {
      state.workout = payload;
    }
  },
  actions: {
    async addExercise({ dispatch }, payload) {
      const exercise = await api.addExercise(payload);
      return dispatch("loadWorkout", exercise.workoutId);
    },
    async delExercise({ dispatch }, { id, workoutId }) {
      await api.delExercise(id);
      return dispatch("loadWorkout", workoutId);
    },
    async addWorkout({ dispatch }) {
      const workout = await api.addWorkout({ date: Date.now() });
      dispatch("loadWorkout", workout.id);
      return workout;
    },
    async loadWorkout({ commit }, id) {
      const workout = await api.getWorkout(id);
      commit(mutations.LOAD_WORKOUT, workout);
      return workout;
    },
    async undoDelExercise({ dispatch }, payload) {
      await api.undoDelExercise(payload.id);
      return dispatch("loadWorkout", payload.workoutId);
    }
  }
});
