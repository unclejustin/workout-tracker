# Contributing

## Project setup

```sh
npm install
```

### Compiles and hot-reloads for development

```sh
npm run serve
```

### Runs and watches json-server for local development

```sh
npm run dev:db
```

### Compiles and minifies for production

```sh
npm run build
```

### Run your tests

```sh
npm run test
```

### Lints and fixes files

```sh
npm run lint
```

### Run your end-to-end tests

```sh
npm run test:e2e
```

### Run your unit tests

```sh
npm run test:unit
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

## Data Structure

Quick note: While we are using document storage we will treat documents as
traditional RDB tables to make reporting easier later.

All tables have the following:

- id: (uuid) unique id for each row
- created_on: (timestamp) when this row was added
- updated_on: (timestamp) when this row was last edited
- deleted_on: (timestamp) when this row was flagged as deleted
  - We use deleted_on so that users may undo their actions instead of being
    to confirm them. Also some rows should never be actually removed
    (exercises and periods for example) because they are referenced historically
    (in the workout exercises)
  - A cleanup function will be used to purge unreferenced items periodically

### exerciseList

- label: (string) the display value for each exercise. ex: "Bench Press"

### periodList

- label: (string) the display value for each period. ex: "4-6 x 3-4 min rest"
- min: (integer) the minimum quantity needed to be considered a successful set
- max: (integer) the maximum quantity allowed before effort should be increased

### workouts

- notes: (text) text blob for any notes

### exercises

- workoutId: (uuid) foreign key to workouts table
- periodListId: (uuid) foreign key to periodList table
- exerciseListId: (uuid) foreign key to exerciseList table
- reps: (number) number of reps performed, using float to allow for
  "time" based reps
- weight: (number) amount of weight used, using float to allow for
  "time" based "weight"

### All tables
